import './assets/main.css'

import { OhVueIcon, addIcons } from "oh-vue-icons";
import { PrTrash, PrHeart, PrHeartFill, PrRefresh } from "oh-vue-icons/icons";
import { createApp } from 'vue'
import App from './App.vue'

addIcons(PrTrash, PrHeart, PrHeartFill, PrRefresh);
const app = createApp(App);
app.component('v-icon', OhVueIcon);
app.mount('#app')
