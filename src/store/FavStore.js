/**
 * src/store/FavStore.js
 *
 * @author Johannes Braun <j.braun@agentur-halma.de>
 * @package vue-quotes
 * @version 2023-08-02
 */
import { reactive, computed } from 'vue';


/**
 * State store
 */
const state = reactive({
	favorites: []
});


/**
 * Getter
 */
const get = () => state.favorites ;


/**
 * Setter
 */
const set = (favorites) => { 
	state.favorites = favorites; 
	persist(); 
}

const clear = () => {
	state.favorites = [];
	persist();
};


const has = (id) => state.favorites.includes(id);


/**
 * Add a favorite
 */
const add = (id) => {
	if (!state.favorites.includes(id)) {
		state.favorites.push(id);
	}
	persist();
}


/**
 * Remove a favorite
 */
const remove = (id) => {
	const index = state.favorites.indexOf(id);
	if (index > 0) {
		state.favorites.splice(index, 1);
	}
	persist();
}


/**
 * Toggle a favorite
 */
const toggle = (id) => {
	const index = state.favorites.indexOf(id);
	if (index > 0) {
		state.favorites.splice(index, 1);
	}
	else {
		state.favorites.push(id);
	}
	persist();
}


/**
 * Persistence
 * Load and save to LocalStorage, later would be some API call to a backend
 */
const load = () => {
	const _favorites = window.localStorage.getItem('favorites');
	if (_favorites) {
		const favorites = JSON.parse(_favorites);
		set(favorites);
	}
}


const persist = () => {
	window.localStorage.setItem('favorites', JSON.stringify(state.favorites));
}



export default () => ({
	set: set,
	get: get,
	add: add,
	has: has,
	remove: remove,
	toggle: toggle,
	clear: clear,
	persist: persist,
	load: load,
	count: computed(() => state.favorites.length),
	name: computed(() => state.name),
	favorites: computed(() => state.favorites)
});
